<?php require './code.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP OOP | Activity</title>
</head>
<body>

    <ul>
        
        <li><?php echo $xioamiMiMonitor->printDetails(); ?></li>
        <li><?php echo $xioamiRedmiNote10Pro->printDetails(); ?></li>
        <li><?php echo $HPBusinessLaptop->printDetails(); ?></li>
    </ul>

    
    <?php $xioamiMiMonitor->setStock(3); ?>
    <?php $xioamiRedmiNote10Pro->setStock(5); ?>
    <?php $HPBusinessLaptop->setCategory("laptops, computers and electronics"); ?>

    <ul>
        <h4>Updated:</h4>
        <li><?php echo $xioamiMiMonitor->getStock(); ?></li>
        <li><?php echo $xioamiRedmiNote10Pro->getStock(); ?></li>
        <li><?php echo $HPBusinessLaptop->getCategory(); ?></li>
    </ul>

</body>
</html>